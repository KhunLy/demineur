(() => {
    const DOM = {
        minesField: document.getElementById('mines-field'),
        easyButton: document.getElementById('easyButton'),
        mediumButton: document.getElementById('mediumButton'),
        hardButton: document.getElementById('hardButton'),
        retryButton: document.getElementById('retry'),
        timer: document.getElementById('timer'),
        remaining: document.getElementById('remaining'),
    }

    const RENDER = {

        initMinesField: (game) => {
            DOM.minesField.innerHTML = '';
            remaining.innerHTML = game.getRemaining();
            for(let i=0; i < game.rows; i++) {
                const row = document.createElement('div');
                DOM.minesField.appendChild(row);
                for(let j=0; j < game.columns; j++) {
                    const tile = document.createElement('div');
                    const button = document.createElement('button');
                    tile.append(button);
                    row.appendChild(tile);
                    // left click
                    button.addEventListener('click', e => {
                        game.pick(j, i);
                        RENDER.renderMinesField(game);
                    });
                    // right click
                    button.addEventListener('contextmenu', e => {
                        e.preventDefault();
                        game.changeMark(j, i);
                        remaining.innerHTML = game.getRemaining();
                        RENDER.renderMinesField(game);
                    });
                }
            }
            RENDER.renderMinesField(game);
        },

        renderMinesField: (game) => {
            for(let i = 0; i < game.rows; i++) {
                for(let j = 0; j < game.columns; j++) {
                    if(game.minesField[i][j].isVisited) {
                        const tile = document.querySelector(`#mines-field>div:nth-child(${i+1})>div:nth-child(${j+1})`);
                        let value = game.minesField[i][j].value;
                        switch (value) {
                            case 0:
                                value = '';
                                break;
                            case 'B':
                                value = '&#128163;';
                                break;
                            case '*':
                                tile.classList.add('exploded');
                                value = '&#128165;';
                                break;
                        }
                        tile.classList.add('color-' + value);
                        tile.innerHTML = value;
                    }
                    else {
                        const btn = document.querySelector(`#mines-field>div:nth-child(${i+1})>div:nth-child(${j+1})>button`);
                        switch (game.minesField[i][j].mark) {
                            case 0:
                                btn.innerHTML = '';
                                break;
                            case 1:
                                btn.innerHTML = '&#128681';
                                this
                                break;
                            case 2:
                                btn.innerHTML = '?';
                                break;
                        }
                    }
                }
            }
        },

        renderTime: time => {
            DOM.timer.innerHTML = UTILS.format(time);
        }
    }

    let game = new Game();

        game.timeChanged = (time) => {
            RENDER.renderTime(time);
        }

        game.init(9,9,10);
        RENDER.initMinesField(game);
        
        DOM.easyButton.addEventListener('click', e => {
            game.init(9,9,10);
            RENDER.initMinesField(game);
        });

        DOM.mediumButton.addEventListener('click', e => {
            game.init(16,16,40);
            RENDER.initMinesField(game);
        });

        DOM.hardButton.addEventListener('click', e => {
            game.init(30,16,99);
            RENDER.initMinesField(game);
        });

        DOM.retryButton.addEventListener('click', e => {
            game.init(game.columns, game.rows, game.bombs);
            RENDER.initMinesField(game);
        });
    })();