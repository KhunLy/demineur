class Game {

    constructor() {
        setInterval(() => {
            if(this.endTime !== null) {
                this.timeChanged(this.endTime);
            }
            else if(this.startTimeStamp) {
                this.timeChanged(Date.now() - this.startTimeStamp)
            } else {
                this.timeChanged(0);
            }
        }, 1);
    }

    init(columns, rows, bombs) {
        this.startTimeStamp = null;
        this.endTime = null;
        this.columns = columns;
        this.rows = rows;
        this.bombs = bombs;
        this.minesField = [];
        this.discovered = 0;

        for(let i = 0; i < this.rows; i++) {
            this.minesField.push([]);
            for(let j = 0; j < this.columns; j++) {
                this.minesField[i].push({value: 0, isVisited: false, mark: 0});
            }
        }
        this.addBombs();
    }


    addBombs() {
        let count = 0;
        while(count < this.bombs) {
            let x = UTILS.getRandomInt(this.columns);
            let y = UTILS.getRandomInt(this.rows);
            if(this.minesField[y][x].value !== 'B') {
                this.minesField[y][x].value = 'B';
                count++;
                UTILS.convoluate(this.minesField, x, y, (cell) => {
                    if(cell.value != 'B') {
                        cell.value++;
                    }
                });
            } 
        } 
    }

    pick(x,y) {
        if(this.minesField[y][x].mark !== 0 || this.endTime !== null)
            return;
        if(!this.startTimeStamp) {
            this.startTimeStamp = Date.now();
        }
        this.minesField[y][x].isVisited = true;
        if(this.minesField[y][x].value === 'B'){
            this.lose(x,y);
        }
        this.discovered++;
        if(this.rows * this.columns - this.discovered === this.bombs)
            this.win();
        if(this.minesField[y][x].value === 0) {
            UTILS.convoluate(this.minesField, x, y, (cell, ax, ay) => {
                if(!cell.isVisited) {
                    this.pick(ax, ay);
                }
            });
        }
    }

    getRemaining() {
        return this.bombs - this.minesField.reduce((acc, current) => {
            return acc + current.filter(c => c.mark === 1).length;
        }, 0);
    }

    visitAll() {
        for(let row of this.minesField) {
            for (let cell of row) {
                cell.isVisited = true;
            }
        }
    }

    win() {
        this.endTime = Date.now() - this.startTimeStamp;
        this.visitAll();
        setTimeout(() => {
            prompt(`Your score: ${UTILS.format(this.endTime)}\nEnter your name for high scores`);
        }, 500);
    }

    lose(x,y) {
        this.endTime = Date.now() - this.startTimeStamp;
        this.minesField[y][x].value = '*';
    }

    changeMark(x,y) {
        // 0: unset 1: flag 2: questionmark
        this.minesField[y][x].mark = (this.minesField[y][x].mark + 1) % 3;
    }

};