const UTILS = {
    /**
     * @description return an integer between 0 and max (not included) 
     * @param {number} max
     * @returns {number}
     */
    getRandomInt: (max) => {
        return Math.floor(Math.random() * max);
    },

    /**
     * @description execute the callback on the adjoining cells
     * @param {any[][]} matrix 
     * @param {number} x 
     * @param {number} y 
     * @param {function} cb 
     */
    convoluate: (matrix, x, y, cb) => {
        for(let i=-1; i<=1; i++) {
            for(let j=-1; j<=1; j++) {
                try {
                    cb(matrix[y+i][x+j], x+j, y+i);
                }
                catch(e) { /*catch if out of bounds*/ }
            }
        }
    },

    format: (time) => {
        const timerTemplate = '__m__:__s__:__ms__';
        const minutes = UTILS.minDigit(Math.floor(time / 60000), 2);
        const seconds = UTILS.minDigit(Math.floor(time / 1000) % 60, 2);
        const milliseconds = UTILS.minDigit(Math.floor(time % 60000), 3);
        return timerTemplate.replace('__m__', minutes)
            .replace('__s__', seconds)
            .replace('__ms__', milliseconds);
       
    },

    minDigit: (val, min) => {
        for(let i=0;i<min;i++) {
            val = '0' + val;
        }
        return val.slice(val.length-min);
    }
}